package metier;

import dto.CompteDTO;
import dto.RetraitDTO;
import exceptions.ExceptionMetier;
import service.ConnectPicsou;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PicsouDAO implements IPicsouDAO{
    // SINGLETON
    private static PicsouDAO dao = new PicsouDAO();
    private PicsouDAO(){}

    public static IPicsouDAO getDAO(){
        return dao;
    }

    // S Q L
    private final static String SQLfindAllComptesByClient =
            "select id_compte, num, titulaire, nom, prenom, date_open, plafond, bloque, solde " +
                    " from Compte c " +
                    " inner join Client cl" +
                    " on cl.id_client = c.titulaire " +
                    " where cl.id_client = ? ";

    private final static String SQLfindCompteById =
            "select id_compte, num, titulaire, nom, prenom, date_open, plafond, bloque, solde " +
                    " from Compte c " +
                    " inner join Client cl" +
                    " on cl.id_client = c.titulaire " +
                    " where c.id_compte = ? ";

    private final static String SQLupdateDebiterCompte =
            "UPDATE compte "
                    +"SET solde =solde + ? "
                    +"WHERE id = ? ";

    private final static String SQLupdateCrediterCompte =
            "update Compte " +
                    " set solde = solde + ? " +
                    "where id_compte = ?";

    private final static String SQLinsertMouvement =
            "insert into Mouvement (id_compte, date_operation, montant, nature) " +
                    " values ( ?, current_timestamp, ?, ?) ";




    //Q5 RETRAIT

    private final static String SQLSommeJoursByCarte =
            "select count(*) as cpt, sum(montant) as somme " +
                    "from Retrait " +
                    " where current_date -10 <= date_retrait " +
                    " and id_carte = ? " +
                    " and etat = 'succes' ";

    private final String SQLupdateNbEssaiCarte =
            " update Carte " +
                    " set nbEssai = ?" +
                    " where id_carte = ?";

    private final String SQLinsertRetrait =
            "insert into Retrait(id_carte, date_retrait, adresseGAB, montant, etat, id_mouv) " +
                    " values ( ?, current_timestamp, ? , ? , 'succes', ? )";

    private final static String SQLfindCarteByID =
            "select id_carte, id_compte, bloque, nbEssai, codeSecret, date_expiration " +
                    " from Carte " +
                    " where id_carte = ? ";

    private final String SQLinsertRetraitAnomalie =
            "insert into Retrait(id_carte, date_retrait, adresseGAB, montant, etat, id_mouv) " +
                    " values ( ?, current_timestamp, ? , ? , ?, null )";



    // METHODES
    @Override
    public List<CompteDTO> findCompteByClient(int idClient) {
        List<CompteDTO> liste = new ArrayList<>();
        new ConnectPicsou();
        Connection conn = ConnectPicsou.getInstance();
        try {
            PreparedStatement pstmt = conn.prepareStatement(SQLfindAllComptesByClient);
            pstmt.setInt(1,idClient);

            ResultSet rs = pstmt.executeQuery();
            while(rs.next()){
                liste.add(new CompteDTO(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getString(4) + " " + rs.getString(5),
                        rs.getDate(6),
                        rs.getDouble(7),
                        rs.getString(8),
                        rs.getDouble(9)
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public CompteDTO findCompteById(int idCompte) throws ExceptionMetier {
        Connection conn;
        conn = ConnectPicsou.getInstance();
        try {
            PreparedStatement pstmt = conn.prepareStatement(SQLfindCompteById);
            pstmt.setInt(1,idCompte);

            ResultSet rs = pstmt.executeQuery();
            if(rs.next()){
               return new CompteDTO(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getString(4) + " " + rs.getString(5),
                        rs.getDate(6),
                        rs.getDouble(7),
                        rs.getString(8),
                        rs.getDouble(9)
                );
            } else {
                throw new ExceptionMetier("findCompteById : idCompte inconnu ");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void credit(int idCompte, double montant) throws ExceptionMetier{
        if(montant <= 0){
            throw new ExceptionMetier("Credit : montant negatif");
        }
        Connection conn = service.ConnectPicsou.getInstance();

        try{
            conn.setAutoCommit(false);
            PreparedStatement psUpdate = conn.prepareStatement(SQLupdateCrediterCompte);

            psUpdate.setInt(2,idCompte);
            psUpdate.setDouble(1, montant);

            int nb =psUpdate.executeUpdate();
            if(nb == 0){
                throw new ExceptionMetier("Credit : id compte inconnu");
            }
            PreparedStatement psInsert = conn.prepareStatement(SQLinsertMouvement);
            psInsert.setInt(1,idCompte);
            psInsert.setDouble(2,montant);
            psInsert.setString(3,"CR");

            nb=psInsert.executeUpdate();

            conn.commit();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            try {
                conn.rollback();
            } catch (SQLException ex) {
                throw new ExceptionMetier("cheum wsh");
            }
        }

    }

    @Override
    public void debit(int idCompte, double montant) throws ExceptionMetier {
        Connection conn = ConnectPicsou.getInstance();

        try{
            conn.setAutoCommit(false);
            PreparedStatement psSelectCompte = conn.prepareStatement(SQLfindCompteById);
            PreparedStatement psUpdate = conn.prepareStatement(SQLupdateDebiterCompte);
            PreparedStatement psInserMvt = conn.prepareStatement(SQLinsertMouvement);

            //Recuperation des données du compte
            psSelectCompte.setInt(1,idCompte);
            ResultSet rs = psSelectCompte.executeQuery();
            if(!rs.next())
                throw new ExceptionMetier("Debit : id compte inconnu");

            double solde = rs.getDouble("solde");
            double plafond = rs.getDouble("plafond");

            if(solde+plafond<montant){
                throw new ExceptionMetier("Debit : solde insuffisant");
            }

            // mise a jour du compte
            psUpdate.setInt(2, idCompte);
            psUpdate.setDouble(1,montant);

            int nb = psUpdate.executeUpdate();

            //creation du mouvement

            psInserMvt.setInt(1,idCompte);
            psInserMvt.setDouble(2,montant);
            psInserMvt.setString(3,"DB");

            nb = psInserMvt.executeUpdate();
            conn.commit();

        } catch (SQLException e) {
            System.err.println(e.getMessage());
            try {
                conn.rollback();
            } catch (SQLException ex) {
                throw new ExceptionMetier("cheum wsh");
            }
        }
    }

    @Override
    public void transfert(int idOrigine, int idDestination, double montant) throws ExceptionMetier{
        if(idOrigine == idDestination) return;
        if(montant <= 0){
            throw new ExceptionMetier("Transfert : montant negatif");
        }

        Connection conn = ConnectPicsou.getInstance();

        try{
            conn.setAutoCommit(false);
            PreparedStatement psSelectCompte = conn.prepareStatement(SQLfindCompteById);
            PreparedStatement psUpdate = conn.prepareStatement(SQLupdateDebiterCompte);
            PreparedStatement psInserMvt = conn.prepareStatement(SQLinsertMouvement);

            //Recuperation des données du compte Origine
            psSelectCompte.setInt(1,idOrigine);
            ResultSet rs = psSelectCompte.executeQuery();
            if(!rs.next())
                throw new ExceptionMetier("Transfert : id compte origine inconnu");

            double solde = rs.getDouble("solde");
            double plafond = rs.getDouble("plafond");

            if(solde+plafond<montant){
                throw new ExceptionMetier("Debit : solde insuffisant");
            }

            //Recuperation des données du compte Destination
            rs.close();
            psSelectCompte.setInt(1,idDestination);
            rs = psSelectCompte.executeQuery();
            if(!rs.next())
                throw new ExceptionMetier("Transfert : id compte destination inconnu");

            // mise a jour du compte
            psUpdate.setInt(2, idDestination);
            psUpdate.setDouble(1,-montant);

            int nb = psUpdate.executeUpdate();

            //creation du mouvement

            psInserMvt.setInt(1,idDestination);
            psInserMvt.setDouble(2,montant);
            psInserMvt.setString(3,"CR");

            nb = psInserMvt.executeUpdate();
            conn.commit();

        } catch (SQLException e) {
            try {
                System.err.println(e.getMessage());
                conn.rollback();
            } catch (SQLException ex) {
                throw new ExceptionMetier("cheum wsh");
            }
        }
    }

    @Override
    public void retrait(int idCarte, String code, String adrGAB, double montant) throws ExceptionMetier {
        if(montant <= 0)
            throw new ExceptionMetier("Retrait : montant négatif ");

        Connection conn = ConnectPicsou.getInstance();

        try{
            conn.setAutoCommit(false);

            //Enregistrement des PreparedStatement
            PreparedStatement psFindCarte = conn.prepareStatement(SQLfindCarteByID);
            PreparedStatement psInsertAnomalie = conn.prepareStatement(SQLinsertRetraitAnomalie);
            PreparedStatement psUpdateNbEssai = conn.prepareStatement(SQLupdateNbEssaiCarte);
            PreparedStatement psSommeJours = conn.prepareStatement(SQLSommeJoursByCarte);
            PreparedStatement psComptesCarte = conn.prepareStatement(SQLfindCompteById);
            PreparedStatement psInsertRetrait = conn.prepareStatement(SQLinsertRetrait);
            PreparedStatement psUpdateCompte = conn.prepareStatement(SQLupdateDebiterCompte);
            PreparedStatement psInsertMvt = conn.prepareStatement(SQLinsertMouvement, Statement.RETURN_GENERATED_KEYS);

            // Controle de la CB ( inconnue, bloquée )
            psFindCarte.setInt(1,idCarte);
            ResultSet rs = psFindCarte.executeQuery();
            if(!rs.next()){/*
                psInsertAnomalie.setInt(1,idCarte);
                psInsertAnomalie.setDouble(3,montant);
                psInsertAnomalie.setString(2,adrGAB);
                psInsertAnomalie.setString(4,"CB inconnue");

                psInsertAnomalie.executeUpdate();*/
                throw new ExceptionMetier("Retrait : carte inconnue");
            }
            // La carte est elle bloquee

            String bloque = rs.getString("bloque");
            if(bloque.equalsIgnoreCase("oui")){
                psInsertAnomalie.setInt(1,idCarte);
                psInsertAnomalie.setDouble(3,montant);
                psInsertAnomalie.setString(2,adrGAB);
                psInsertAnomalie.setString(4,"CB bloquee");

                psInsertAnomalie.executeUpdate();
                conn.commit();
                throw new ExceptionMetier("Retrait : carte bloquee");
            }

            //Controle code
            String codeSecret = rs.getString("codeSecret");
            int nbEssai = rs.getInt("nbEssai");
            if (!codeSecret.equalsIgnoreCase(code)) {
                psInsertAnomalie.setInt(1, idCarte);
                psInsertAnomalie.setDouble(3, montant);
                psInsertAnomalie.setString(2, adrGAB);
                psInsertAnomalie.setString(4, "codeSecret");

                psInsertAnomalie.executeUpdate();

                nbEssai = nbEssai +1;
                psUpdateNbEssai.setInt(2,idCarte);
                psUpdateNbEssai.setInt(1,nbEssai);
                conn.commit();
                throw new ExceptionMetier("Retrait : code erroné");
            } else {

                psUpdateNbEssai.setInt(2,idCarte);
                psUpdateNbEssai.setInt(1,0);
                psUpdateNbEssai.executeUpdate();

                int idCompte = rs.getInt("id_compte");
                psSommeJours.setInt(1, idCompte);
                ResultSet rs1 = psSommeJours.executeQuery(); //ERROR

                rs1.next();
                int cpt = rs1.getInt("cpt");

                double somme = (cpt==0?0.0:rs1.getDouble("somme"));


                if(somme >= 500.0){
                    psInsertAnomalie.setInt(1, idCarte);
                    psInsertAnomalie.setDouble(3, montant);
                    psInsertAnomalie.setString(2, adrGAB);
                    psInsertAnomalie.setString(4, "10Jours");

                    psInsertAnomalie.executeUpdate();

                    conn.commit();
                    throw new ExceptionMetier("Retrait : 10 Jours glissants");
                }

                psComptesCarte.setInt(1,idCompte);
                ResultSet rs2 = psComptesCarte.executeQuery();
                if(rs2.next())
                    throw new ExceptionMetier("Retrait : id compte inconnu");


                double solde = rs2.getDouble("solde");
                double plafond = rs2.getDouble("plafond");

                if(solde+plafond<montant){
                    psInsertAnomalie.setInt(1, idCarte);
                    psInsertAnomalie.setDouble(3, montant);
                    psInsertAnomalie.setString(2, adrGAB);
                    psInsertAnomalie.setString(4, "10Jours");

                    psInsertAnomalie.executeUpdate();
                    conn.commit();
                    throw new ExceptionMetier("Retrait : solde insuffisant");

                }

                //Ajout mouvement
                psInsertMvt.setInt(1,idCompte);
                psInsertMvt.setDouble(2,montant);
                psInsertMvt.setString(3,"CB");

                psInsertMvt.executeUpdate();

                //RECUPERATION DE LA CLEF PRIMAIRE AUTO GENEREE
                ResultSet rsInsertMvt = psInsertMvt.getGeneratedKeys();
                rsInsertMvt.next();
                int key = rsInsertMvt.getInt(1);

                //Ajout retrait

                psInsertRetrait.setInt(1,idCarte);
                psInsertRetrait.setString(2,adrGAB);
                psInsertRetrait.setDouble(3,montant);
                psInsertRetrait.setInt(4,key);
                psInsertRetrait.executeUpdate();

                //mise a jour comtpe : solde
                psUpdateCompte.setInt(2,idCompte);
                psUpdateCompte.setDouble(1,montant);

                psUpdateCompte.executeUpdate();

                conn.commit();

            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            try {
                conn.rollback();
            } catch (SQLException ex) {
                throw new ExceptionMetier("cheum wsh");
            }
        }
    }

    @Override
    public List<RetraitDTO> findRetraitByCarte(int idCarte) throws ExceptionMetier {
        return null;
    }

    public static void main(String[] args) {
        PicsouDAO picsou = new PicsouDAO();

        for(CompteDTO compte : dao.findCompteByClient(1)){
            System.out.println(compte);
        }

        try{
            System.out.println("______________");
            System.out.println(dao.findCompteById(3));

            //dao.retrait(1,"1234","ANZIN", 200.0);
            dao.retrait(2,"5678","LENS", 1);
        } catch (ExceptionMetier exceptionMetier) {
            System.err.println(exceptionMetier.getMessage());
        }



    }
}
