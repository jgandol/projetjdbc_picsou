package metier;


import dto.CompteDTO;
import dto.RetraitDTO;
import exceptions.ExceptionMetier;

import java.util.List;

public interface IPicsouDAO {
    //Gestion des comptes
    public List<CompteDTO> findCompteByClient(int idClient);
    public CompteDTO findCompteById(int idCompte) throws ExceptionMetier;
    public void credit(int idCompte, double montant) throws ExceptionMetier;
    public void debit(int idCompte, double montant) throws ExceptionMetier;
    public void transfert(int idOrigine, int idDestination, double montant) throws ExceptionMetier;

    public void retrait(int idCarte, String code, String adrGAB, double montant) throws ExceptionMetier;
    public List<RetraitDTO> findRetraitByCarte(int idCarte) throws ExceptionMetier;

    //Consultation des mouvements :
    //public List<MouvementDTO> findMouvementsByCompte(int idCompte);
}
