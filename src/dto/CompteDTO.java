package dto;

import java.io.Serializable;
import java.sql.Date;

public class CompteDTO implements Serializable {
    // numero de version
    private static final long serialVersionUID =1L;

    // attributs d'instance perinents
    private int id;
    private String numero;

    private int idTitulaire;
    private String titulaire;

    private Date date_ouverture;
    private double plafond;
    private String bloque;
    private double solde;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNumero() {
        return numero;
    }
    public void setNumero(String numero) {
        this.numero = numero;
    }
    public int getIdTitulaire() {
        return idTitulaire;
    }
    public void setIdTitulaire(int idTitulaire) {
        this.idTitulaire = idTitulaire;
    }
    public String getTitulaire() {
        return titulaire;
    }
    public void setTitulaire(String titulaire) {
        this.titulaire = titulaire;
    }
    public Date getDate_ouverture() {
        return date_ouverture;
    }
    public void setDate_ouverture(Date date_ouverture) {
        this.date_ouverture = date_ouverture;
    }
    public double getPlafond() {
        return plafond;
    }
    public void setPlafond(double plafond) {
        this.plafond = plafond;
    }
    public String getBloque() {
        return bloque;
    }
    public void setBloque(String bloque) {
        this.bloque = bloque;
    }
    public double getSolde() {
        return solde;
    }
    public void setSolde(double solde) {
        this.solde = solde;
    }

    public CompteDTO(int id, String numero, int idTitulaire, String titulaire, Date date_ouverture, double plafond, String bloque, double solde) {
        super();
        this.id = id;
        this.numero = numero;
        this.idTitulaire = idTitulaire;
        this.titulaire = titulaire;
        this.date_ouverture = date_ouverture;
        this.plafond = plafond;
        this.bloque = bloque;
        this.solde = solde;
    }

    @Override
    public String toString() {
        return "compteDTO{" +
                "id=" + id +
                ", numero='" + numero + '\'' +
                ", idTitulaire=" + idTitulaire +
                ", titulaire='" + titulaire + '\'' +
                ", date_ouverture=" + date_ouverture +
                ", plafond=" + plafond +
                ", bloque='" + bloque + '\'' +
                ", solde=" + solde +
                '}';
    }
}
