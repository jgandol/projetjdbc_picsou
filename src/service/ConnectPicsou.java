package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectPicsou {

    private final String URL = "jdbc:mysql://localhost:3306/Picsou";
    private final String USER = "jeremie";
    private final String PW = "admin";

    private static Connection INSTANCE;

    public ConnectPicsou(){
        try{
            INSTANCE = DriverManager.getConnection(URL,USER,PW);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getInstance(){
        if(INSTANCE == null){
            new ConnectPicsou();
        }
        return INSTANCE;
    }

    public static void main(String[] args) {
        ConnectPicsou.getInstance();
    }

}
