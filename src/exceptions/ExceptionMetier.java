package exceptions;

public class ExceptionMetier extends Exception {

    public ExceptionMetier(){
        super();
    }

    public ExceptionMetier(String message){
        super(message);
    }
}
